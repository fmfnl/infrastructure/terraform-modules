terraform {
  required_version = ">= 1.3.0"

  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.11"
    }

    local = {
      source = "hashicorp/local"
      version = "2.3.0"
    }

    ct = {
      source = "poseidon/ct"
      version = "0.11.0"
    }

    null = {
      source = "hashicorp/null"
      version = "3.2.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.12.0"
    }
  }
}
